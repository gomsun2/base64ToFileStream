# base64ToFileStream

Download: [base64ToStream.exe](./release/base64ToStream.exe)

base64ToFileStream은 gmail에서 차단된 첨부 파일을 복구하는데 사용할 수 있습니다.

![base64ToFileStream](base64ToFileStream.png)

## 사용방법

1. 차단된 첨부파일이 포함된 메일에서, 오른쪽 `... 버튼`을 클릭, `원본보기`를 클릭합니다.<br>![01](designRes/step_01.png)
1. `원본 메일 다운로드` 를 클릭 합니다.<br> ![02](designRes/step_02.png)
1. 다운로드 된 파일을 base64ToFileSteram에 드래그 후 드롭합니다.